# Energy badge

_A small badge that measures the resources used by a page_

The goal of this project is to provide a way for a website to transparently inform its users about its carbon footprint through a badge.

# Why energy badge ?
Climate change is one of the largest event threatening humanity, and the web has a lot to do with it. In fact, an estimated 416.2TWh are consummed each year by the infrastructure of the internet: this includes the data centers, the devices we hold in our hands, and the network infrastructure in between.

Some work have already been done in order to extend awareness and bring more transparency to the general public:
- A static analyser for web pages (https://www.websitecarbon.com/)
- A browser extension (https://theshiftproject.org/en/carbonalyser-browser-extension/)

The goal of energy badge is to extends these works in another new way.

# How does this work?

Once loaded in the browser, energy badge uses the `Performance API` (https://developer.mozilla.org/en-US/docs/Web/API/Performance) in order to make various measurements. No external requests are made, and all the necessary computations are performed locally.

## Detailed model

You can find the detailed model we are using in the wiki link below:

https://gitlab.com/HadrienRenaud/energy-badge/-/wikis/Model-researchs

# How to use this badge ?

Usage is very simple! You simply have to include:
`<script src="https://cdn.jsdelivr.net/npm/energy-badge@0.0.5/dist/index.js"></script>`
in the html of the webpage.

