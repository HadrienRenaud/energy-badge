type AnalysisResult = number; // in gCO2e
type Callback = (x: AnalysisResult) => void;

let callbacks: Callback[] = [];
let lastAnalysisResult: AnalysisResult | null = null;

export function registerAnalyserCallback(cb: Callback) {
    callbacks.push(cb);
    if (lastAnalysisResult) cb(lastAnalysisResult);
}

const ENERGY_NETWORK_BY_BYTE = 0.18E-9; // in kWh/byte
// source : The Shift Project

const CARBON_BY_ENERGY = 287; // in gCO2e/kWh
// source : EEA

type AcceptedEntry = PerformanceResourceTiming & PerformanceNavigationTiming;
const ACCEPTED_ENTRY_TYPE = ["resource", "navigation"]

// This function analyses an entry and return the number of gCO2e consumed by this entry
// Currently, it only uses the transfer size.
// CF : https://gitlab.com/HadrienRenaud/energy-badge/-/wikis/Model-researchs
function analyseEntry(entry: AcceptedEntry): AnalysisResult {
    let {transferSize} = entry as AcceptedEntry;

    return CARBON_BY_ENERGY * (
        transferSize * ENERGY_NETWORK_BY_BYTE
    )
}


// This function returns the number of gCO2e used by the page since its load.
function analyse(): AnalysisResult {
    const entries = window.performance.getEntries()
        .filter(
            (e: PerformanceEntry) => ACCEPTED_ENTRY_TYPE.indexOf(e.entryType) != -1
        ) as AcceptedEntry[];

    return entries.map(analyseEntry).reduce((a, b) => a + b, 0)
}

// This function trigger an analyse and propagate the result
function main() {
    let result = analyse();
    lastAnalysisResult = result;
    for (let cb of callbacks) {
        cb(result);
    }
}

// Analyse and propagate every second
setInterval(main, 1000);

