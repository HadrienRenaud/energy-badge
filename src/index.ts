
import {registerAnalyserCallback} from "./analyse"
import {importCSS} from "./helpers"

importCSS('index.css');
import "./co2-badge.css";

const SIGNIFICANTS_DIGITS = 2; // Number of significant digits to be used.

const currentNode = document.currentScript;
let displayedNode = document.createElement("div");
displayedNode.setAttribute("id", "co2-badge");

let infoNode = document.createElement("a");
infoNode.setAttribute("id", "info");
infoNode.setAttribute("href", "https://gitlab.com/HadrienRenaud/energy-badge/-/wikis/Model-researchs");
infoNode.innerHTML = "CO<sub>2</sub>&nbsp;Badge";

let dataNode = document.createElement("span");
dataNode.setAttribute("id", "data");
dataNode.setAttribute("class", "text");
dataNode.innerHTML = "Since loading, this page emitted 0 grams of CO<sub>2</sub>&nbsp;equivalent.";

displayedNode.appendChild(infoNode);
displayedNode.appendChild(dataNode);
currentNode.replaceWith(displayedNode);

function display(result: number) {
    dataNode.innerHTML = "Since loading, this page emitted " + result.toExponential(SIGNIFICANTS_DIGITS-1) + " grams of CO<sub>2</sub>&nbsp;equivalent.";
}

function bundle() {
    registerAnalyserCallback((x) => {
        console.log("Analyser found that the emissions of this page are " + x + " grams of co2 equivalent.\nLearn more at https://gitlab.com/HadrienRenaud/energy-badge/-/wikis/Model-researchs\n")
    });
    registerAnalyserCallback(display);
}

bundle();

