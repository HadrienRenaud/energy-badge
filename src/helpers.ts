// Import the file filename (as bundled) into the document
// This is very very ugly
// But I don't know how to do it otherwise
export function importCSS(filename: string) {
    const headID = document.getElementsByTagName("head")[0];
    const fileref = document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")

    const currentHref = document.currentScript.getAttribute("src")
    const href = currentHref.split("/").slice(0, -1).join("/") + "/" + filename;
    fileref.setAttribute("href", href);

    headID.appendChild(fileref);
}
